When people hear for the first time the term `\DebianMed' there are usually
two kinds of misconceptions.  Let us dispel these in advance, so as to
clarify subsequent discussion of the project.

People familiar with Debian as a large distribution of Free Software
usually imagine Debian Med to be some kind of customised derivative of
Debian tailored for use in a medical environment.  Astonishingly, the
idea that such customisation can be done entirely {\em within} Debian
itself is not well known and the technical term {\em Debian Pure Blend}
seems to be sufficiently unknown outside of the Debian milieu that many
people fail to appreciate the concept correctly.  There are no separate
repositories like Personal Package Archives (PPA) as introduced by
Ubuntu for additional software not belonging to the official
distribution or something like that -- a Debian Pure Blend (as the term
'pure' implies) is Debian itself and if you have received Debian you
have full \DebianMed at your disposal.  There are other 
Blends inside Debian like Debian Science, Debian Edu, Debian GIS and others.

People working in the health care professions sometimes acquire
another misconception about \DebianMed, namely that \DebianMed is some
kind of software primarily dedicated to managing a doctor's practice.
Sometimes people even assume that people assume the
\DebianMed team actually develops this software.  However, the
truth about the \DebianMed team is that we are a group of Debian
developers hard at work incorporating {\em existing} medical software right
into the Debian distribution.  We can hardly include software
that does not yet exist and so we try to cherry-pick the
Free Software that is useful for medical care tasks.  Because
we do not restrict ourselves to pure practice management, Debian now
contains not only two practice management systems (GNUmed and
FreeMedForms), but much other software useful in medical care as well.
Bioinformatics (next generation sequencing, phylogeny, etc.) and
medical imaging are two such fields of particular importance.
We do also maintain a to-do list
of software that should be included as time and manpower permits.  For
instance one of the main candidates on the horizon is VistA - an
integrated health information system which is successfully running in
several hospitals.

Having clarified the misconceptions about \DebianMed, let us now
give a positive definition: \DebianMed is a pure subset of Debian that
tries to integrate all existing Free Software that might be used in any
subfield of medical care.  In other words it is a Debian
Pure Blend covering medical and microbiological software.

The \DebianMed project started in 2002 with a very small team but in the
meantime has evolved to include about 25 participants, each working in
various capacities to increase the pool of available, relevant medical
software in the Debian repositories and maintain Debian's infrastructure
specifically targeting at Debian Pure Blends needs, enabling efficient
access to said resources.  Due to their involvement with the Debian Med
project, ten contributors have become Debian developers.  Thus the
Debian Med project serves to recruit talented new developers into the
Debian fold.

\DebianMed to a certain extend serves as a missing link between the
developers of Free Software medical applications (in Debian slang
`upstream') and the end users who install them.  
The project significantly eases the effort of software installation on a
system by using so called 'metapackages'.  Metapackages are making use
of an important feature of the Debian packaging system to define
relations between packages (containig software that depends from each
other).  So a metapackage does not actually contain some software the
user will run but it rather defines dependencies from other software.
Metapackages assembled by the \DebianMed team denote collections of
software that are commonly used together to accomplish a given sort of
task---medical tasks, in this case.

In addition \DebianMed attends to security problems and bugfixes and quite frequently
we support upstream developers with patches to enhance their software.
Because of this we have established rather good connections with the
authors of the software in our area and encourage the formation of a
large community around the Free Software universe of medical resources
-- on the solid and well known technical basis of the Debian
distribution.

Once people have understood the \DebianMed idea they often begin to
fantasise:  "Oh, that's nice.  I could accomplish my medical IT tasks
without spending any money anymore, wonderful." Well, that is indeed a
pleasant dream, but honesty requires us to provide you with a reality
check.  Yes, you get the software to use with no licensing costs to pay,
and this will certainly reduce your effective costs. We also should
point out that it is not only free of charge but high quality as well,
which can be verified by inspecting the code.  We will never play dirty
tricks with your data and we will never be the stumbling stone when it
comes to porting your data from one system to another.

Nonetheless the fact remains that healthcare on the one hand, and
computer science on the other, are complex fields of endeavor and you
will rarely find professionals who are experts in both fields at the
same time.  It is said that knowledge is power, and that with power
comes responsibility.
%
% remark of Wes Davidson <davidson@ling.ohio-state.edu>:
% perhaps there exists a fruitful analogy along the following lines:
%
% drugs:pharmacy:medical-professional:patient
%  ::
%  software:Debian-Med:IT-professional:IT-system
%
But whenever you rely on an IT system, {\em someone} must take
responsibility for its maintenance.  So if you are working in health
care you are responsible for the health of the patient and not primarily
for the health of your IT systems.  In other words:  You must ultimately
spend some money to hire an IT expert who will maintain your IT
infrastructure and provide the service you need for your work.

\DebianMed targets at your IT service provider, equipping them with high
quality software free of charge with strong established ties to the
community that develops it.  Freely available source code unencumbered
by proprietary licenses means they can adapt the code to the needs of
their clients and become part of the fascinating world of Free Software
by passing back their enhancements to the upstream developers.  These
unique features of Free Software put those IT service providers selling
support for Free Software in medical care in a very good position
compared to those selling proprietary software:  They can provide better
service for less money.  In the final analysis, their clients in the
medical professions will profit as well from better service for less
money, and this where reality approaches, but does not attain, the
fantasy of a cost-free medical IT system.

In summary, \DebianMed strives to maintain the foundation on which
you can build a cheaper, more reliable and more flexible IT
infrastructure for medical care while at the same time gaining independence
from proprietary companies.

As you probably know, there are many derivatives of Debian (Ubuntu,
Linux Mint, etc.) -- it is actually the most derived existing Linux
distribution.  The advantage of working strictly within Debian has the
positive side effect that all its derivatives profit from the work of
the \DebianMed team as well.  For example, there even exist strong
connections to derivatives like
\printurl{http://nebc.nerc.ac.uk/tools/bio-linux/bio-linux-7-info}{BioLinux}
an Ubuntu derivative which works closely with \DebianMed to profit from
our work via Ubuntu.

% How do we work together

How to get more information about the project:
\begin{itemize}
   \item \printurl
           {debian-med.alioth.debian.org/tasks}
           {Overview about packaged software (and software we are working on)}
   \item \printurl
           {debian-med.alioth.debian.org/doc/policy.html}
           {Policy if you want to start joining the development process}
   \item \printurl
           {people.debian.org/\~tille/talks}
           {Several talks about \DebianMed and related topics, some also video recorded}
\end{itemize}

Many thanks go to all contributors to Debian and \DebianMed,
\emph{i.e.}, the providers of the packages, the developers of the Free
Software and to all those who donate time and resources for the project.

