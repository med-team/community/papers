#!/usr/bin/make -f

text=debian-med-bio

DISTDIR=debian-bio-0.2

%.html: %.tex
	echo "Unfortunately there is no latex 2 html solution for the moment."

all: pdf
# html dvi

html: $(text).html
$(text).html: $(text).tex
	make clean
	make bbl
	cp -a $(text).tex $(text).tex.save
	sed -i \
	    -e "s/\(\\documentclass{\)acm_proc_article-sp/\1article/" \
	    -e "s/\(\\\\titlenote{.*\)\\\\printurl{[^}]*}{[^}]*}/\1/" \
	    -e "/\\\\titlenote{/{;N;N;s/\\\\titlenote{[^}]*}//;}" \
	    -e "/\\\\affaddr{.*}/d" \
	    -e "/\\\\email{.*}/d" \
	    -e "/\\\\alignauthor/d" \
	    -e "/\\\\numberofauthors/d" \
	    $(text).tex
	sed -i \
	    -e "/\\\\author{/{;N;N;N;N;N;N;N;N;s/\\\\author{[^}]*}//;}" \
	    -e "s/\\\\halfsloppy//" \
	    -e "/\\\\printurl/{;N;N;s?\\\\printurl{\([^}]*\)}\({[^}]*}\)?\\\\htmladdnormallink\2{http://\1}?g;}" \
	    -e "s/\(\\\\begin{tabular}{.*\)p{[^}]*}\(.*\)/\1l\2/" \
	    -e "/\\\\conferenceinfo/{;N;N;s/\\\\conferenceinfo{[^}]*}{[^}]*}/\\\\htmladdnormallink{Find the paper in PDF and more information about the conference here}{..}/;}" \
	    -e "s/\\\\CopyrightYear{[^}]*}//" \
	    $(text).tex
	sed -i \
	    -e "/\\\\ifshowURL/{;N;N;N;s?\\\\ifshowURL.*|\(http://[^,]*\)[[:space:],]*\(http://[^|]*\)|}\\\\fi?\\\\htmladdnormallink{\1}{\1}, \\\\htmladdnormallink{\2}{\2}?g;}" \
	    $(text).bbl
	sed -i \
	    -e "/\\\\ifshowURL/{;N;N;N;s?\\\\ifshowURL.*|\(http://[^|]*\)|}%*[[:space:]]*\\\\fi?\\\\htmladdnormallink{\1}{\1}?g;}" \
	    $(text).bbl
	sed -i \
	    -e "/\\\\ifshowURL/{;N;N;s?\\\\ifshowURL.*|\(http://[^|]*\)|}%*[[:space:]]*\\\\fi?\\\\htmladdnormallink{\1}{\1}?g;}" \
	    -e "/\\\\htmladdnormallink/s/%/\\\\%/g" \
	    $(text).bbl
	sed -i \
	    -e "/\\\\ifx[[:space:]]\\\\.*[[:space:]]\\\\fi/d" \
	    $(text).bbl
	latex2html -local_icons $(text).tex
	touch $(text).html
	##mv $(text).tex $(text).tex_used4html
	mv $(text).tex.save $(text).tex

bbl: $(text).bib
	pdflatex $(text).tex
	bibtex debian-med-bio

dvi: $(text).dvi
$(text).dvi: $(text).tex
	make clean
	convert earthkeyring.png earthkeyring.eps
	latex $(text).tex
	bibtex $(text)
	latex $(text).tex
	latex $(text).tex

pdf: $(text).pdf
$(text).pdf: $(text).tex paper-text.tex $(text).bib
	make clean
	pdflatex $(text).tex
	bibtex $(text)
	pdflatex $(text).tex
	pdflatex $(text).tex

pdfllncs: $(text)-llncs.pdf
$(text)-llncs.pdf: $(text)-llncs.tex paper-text.tex
	make clean
	pdflatex $(text)-llncs.tex
	bibtex $(text)-llncs
	pdflatex $(text)-llncs.tex
	pdflatex $(text)-llncs.tex

dvips: $(text)-dvips.pdf
$(text)-dvips.pdf: $(text).dvi
	dvips -o $(text)-dvips.ps $(text).dvi
	ps2pdf $(text)-dvips.ps
	rm $(text)-dvips.ps

distclean: clean
	rm -f $(text)*.html $(text).pdf $(text)-dvips.pdf $(text)-llncs.pdf
	rm -rf $(DISTDIR) debian-med-bio

clean:
	rm -f $(text)*.log $(text)*.aux $(text)*.dvi $(text)*.toc $(text)*.out $(text)*.bbl $(text)*.blg
	rm -f *.log
	rm -f earthkeyring.eps

dist:
	mkdir -p $(DISTDIR)/paper
	cp -a *.tex *.css Makefile $(DISTDIR)/paper
	tar -czf $(DISTDIR).tgz $(DISTDIR)
	rm -rf $(DISTDIR)
